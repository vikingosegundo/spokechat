//
//  misc.swift
//  SpokeChat
//
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import Foundation

extension Sequence {
    func sorted<T: Comparable>(by keyPath: KeyPath<Element, T>) -> [Element] { sorted { $0[keyPath: keyPath] < $1[keyPath: keyPath] } }
}

func documentsFolderURL(with fileManger: FileManager = .default) -> URL { fileManger.urls(for: .documentDirectory, in: .userDomainMask)[0] }

func passwordFormatChecker(_ password: String) -> Bool {
    password.trimmingCharacters(in: .whitespacesAndNewlines).count > 4
}

func usernameFormatChecker(_ username: String) -> Bool {
    username.trimmingCharacters(in: .whitespacesAndNewlines) != ""
}

func humanDate(_ date: Date) -> String{
    let formatter = DateFormatter()
    formatter.dateFormat = DateFormatter.dateFormat(fromTemplate: "hMdmY", options: 0, locale: Locale.current)
    return formatter.string(from: date)
}

func getDate(_ date: Date) -> String{
    let formatter = DateFormatter()
    formatter.dateFormat = "dd/M/yyyy"
    return formatter.string(from: date)
}

func getTime(_ date: Date) -> String{
    let formatter = DateFormatter()
    formatter.dateFormat = "H:mm"
    return formatter.string(from: date)
}

func JIDToName(_ JID: String) -> String{
    return JID.replacingOccurrences(of: "@spokechat.ch", with: "")
}

func nameToJID(_ name: String) -> String{
    return name + "@spokechat.ch"
}

func unreadMessageIn(chat: Chat) -> Int { chat.messages.filter { !$0.wasReadBefore  }.count }

func documentsFolder() -> URL {
    URL(string: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])!
}
