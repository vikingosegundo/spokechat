//
//  ReplacingContactGateway.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 09/08/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

enum ReplacingContactError: Error {
    case error
}

protocol ReplacingContactGateway {
    func replace(old: ChatContact, new: ChatContact, callback: @escaping (Result<AppState, ReplacingContactError>) -> ())
}
