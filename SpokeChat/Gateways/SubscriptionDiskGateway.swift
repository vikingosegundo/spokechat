//
//  SubscriptionDiskGateway.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 06/08/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import Foundation

class SubscriptionDiskGateway {
    
    enum DiskError: Error {
        case savingFailed(ChatContact)
    }
    
    init(fileManager: FileManager) {
        self.fileManager = fileManager
    }
    
    func save(subscription: ChatContact, callback: @escaping (Result<ChatContact, DiskError>) -> ()) {
        
        let fileName = "subscriptions.txt"
        let path = documentsFolder().appendingPathComponent(fileName, isDirectory: false).path
        if !fileManager.fileExists(atPath: path) {
            fileManager.createFile(atPath: path, contents: nil, attributes: nil)
        }
        
        do {
            let data = try String(contentsOfFile: path, encoding: .utf8)
            let lines =  Set(data.components(separatedBy: .newlines))
            let newLines = (lines + ["handle: \(subscription.handle)"]).joined(separator: "\n")
            try newLines.write(toFile: path, atomically: true, encoding: .utf8)
            
            callback(.success(subscription))
            
        } catch {
            callback(.failure(.savingFailed(subscription)))
        }
    }
    
    private let fileManager: FileManager
}
