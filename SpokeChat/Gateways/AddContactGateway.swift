//
//  AddContactGateway.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 06/08/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

protocol AddContactGateway: class {
    func add(contact: ChatContact, callback: @escaping (Result<ChatContact, AddContactError>) -> ())
    var contactRequestReceived: ((ChatContact) -> ())? { set get }
    var contactUpdated: ((ChatContact) -> ())? { set get }
}

enum AddContactError: Error {
    case failed(underlaying: Error)
}
