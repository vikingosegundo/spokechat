//
//  LogInGateWay.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 09/07/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import Foundation

enum LogInGatewayError: Error {
    case failed
    case noCert(Data, String, SecTrustResultType)
}

protocol LoginGateway: class {
    var loginSuccessfull: (() -> ())? { set get }
    func logIn(credentials: ChatCredentials)
}
