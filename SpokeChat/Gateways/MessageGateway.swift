//
//  MessageSendingGateway.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 19/07/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

protocol MessageGateway: class {
    func sendMessage(_ data:ChatMessage)
    var chatMessageReceived: ((ChatMessage) -> ())?  { set get }
}
