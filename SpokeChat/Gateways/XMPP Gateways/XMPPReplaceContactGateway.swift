//
//  ReplaceContactGateway.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 09/08/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import TigaseSwift

class XMPPReplaceContactGateway: ReplacingContactGateway {
    
    init(xmppStack:XMPPStack ,appStateAccessor: AppStateAccessor) {
        self.appStateAccessor = appStateAccessor
        self.xmppStack = xmppStack
    }
    
    private let appStateAccessor: AppStateAccessor
    private let xmppStack: XMPPStack
    
    func replace(old: ChatContact, new: ChatContact, callback: @escaping (Result<AppState, ReplacingContactError>) -> ())  {
        let rosterModule:RosterModule = xmppStack.client.modulesManager.getModule(RosterModule.ID)!
        if let rosterItem = rosterModule.rosterStore.get(for: JID(old.handle)) {
            let nr = RosterItem(jid: rosterItem.jid, name: new.nickname, subscription: rosterItem.subscription, annotations: rosterItem.annotations)
            rosterModule.rosterStore.update(item: nr, name: new.nickname) { (sucessstanza) in
                let newstate = self.appStateAccessor.appState.alter(.contacts(self.appStateAccessor.appState.chatContacts.filter{ $0.handle != old.handle} + [new]))
                callback(.success(newstate))
            } onError: { (error) in
                callback(.failure(.error))
            }
        }
    }
}
