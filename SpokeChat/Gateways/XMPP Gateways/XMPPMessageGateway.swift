//
//  XMPPMessageGateway.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 09/08/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import TigaseSwift

class XMPPMessageGateway: MessageGateway {
    init(xmppStack: XMPPStack) {
        self.xmppStack = xmppStack
    }
        
    func sendMessage(_ data:ChatMessage) {
        let messageModule: MessageModule = xmppStack.client.modulesManager.getModule(MessageModule.ID)!
        let chat = messageModule.createChat(with: JID( data.to.handle))
        _ = messageModule.sendMessage(in: chat!, body: data.body)
    }
    
    var chatMessageReceived: ((ChatMessage) -> ())? {
        set { xmppStack.chatMessageReceived = newValue }
        get {xmppStack.chatMessageReceived }
    }

    private let xmppStack: XMPPStack
}
