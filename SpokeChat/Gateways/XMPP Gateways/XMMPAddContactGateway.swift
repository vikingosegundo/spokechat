//
//  XMMPAddContactGateway.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 09/08/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import TigaseSwift

class XMMPAddContactGateway: AddContactGateway, XmppServiceEventHandler {
    
    init(xmppStack: XMPPStack) {
        self.xmppStack = xmppStack
        xmppStack.add(eventHandler: self)
    }
    
    let events: [Event] = [PresenceModule.SubscribeRequestEvent.TYPE, RosterModule.ItemUpdatedEvent.TYPE]

    func handle(event: Event) {
        switch event {
        case let e as PresenceModule.SubscribeRequestEvent:
            guard
                let from = e.presence.from
            else { return }
            
            let sender  = from.bareJid;
            self.contactRequestReceived?(ChatContact(handle: sender.stringValue).alter(.nickname(e.presence.nickname)))
            
        case let e as RosterModule.ItemUpdatedEvent:
            print(e)
            guard let item = e.rosterItem else { return }
            let contact =
                ChatContact(
                    handle: item.jid.bareJid.stringValue
                ).alter(.nickname(item.name))
            self.contactUpdated?(contact)
            
        default:
            break
        }
    }
    
    var contactRequestReceived: ((ChatContact) -> ())?
    var contactUpdated: ((ChatContact) -> ())?

    func add(contact: ChatContact, callback: @escaping (Result<ChatContact, AddContactError>) -> ()) {
        
        let rosterModule:RosterModule = xmppStack.client.modulesManager.getModule(RosterModule.ID)!;
        if let rosterItem = rosterModule.rosterStore.get(for: JID(contact.handle)) {
                rosterModule.rosterStore.update(item: rosterItem,
                                                name: contact.nickname,
                                                onSuccess: { (stanza: Stanza) in
                                                    print(stanza)
                                                    callback(.success(contact))
                                                    self.updateSubscriptions(jid: JID(contact.handle))
                                                },
                                                onError: {(error: ErrorCondition?) in
                                                    if let error = error {
                                                    callback(.failure(.failed(underlaying: error)))
                                                    }
                                                });
        } else {
            rosterModule.rosterStore.add(jid: JID(contact.handle), name: contact.nickname, onSuccess: { (stanza: Stanza) in
                print(stanza)
                self.updateSubscriptions(jid: JID(contact.handle))
                callback(.success(contact))
                
            }, onError: {(error: ErrorCondition?) in
                if let error = error {
                callback(.failure(.failed(underlaying: error)))
                }
            })
        }
        
    }
    fileprivate func updateSubscriptions(jid: JID) {
        let rosterModule:RosterModule = xmppStack.client.modulesManager.getModule(RosterModule.ID)!;
        if let presenceModule: PresenceModule = xmppStack.client.modulesManager.getModule(PresenceModule.ID) {
            guard let rosterItem = rosterModule.rosterStore.get(for: jid) else {
                return;
            }
            if !rosterItem.subscription.isTo {
                presenceModule.subscribe(to: jid, preauth: nil);
            }
            if !rosterItem.subscription.isFrom {
                presenceModule.subscribed(by: jid);
            }

        }
    }
    private let xmppStack: XMPPStack
}
