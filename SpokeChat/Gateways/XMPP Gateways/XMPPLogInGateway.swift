//
//  XMPPLogInGateway.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 09/08/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import TigaseSwift

class XMPPLogInGateway: LoginGateway {
    
    init(xmppStack: XMPPStack) {
        self.xmppStack = xmppStack
    }
    
    private let xmppStack: XMPPStack
    var loginSuccessfull: (() -> ())?

    func logIn(credentials: ChatCredentials) {
        xmppStack.client.connectionConfiguration.setUserJID(BareJID(credentials.username))
        xmppStack.client.connectionConfiguration.setUserPassword(credentials.password)
        loginSuccessfull?()
        xmppStack.client.login()
    }
}
