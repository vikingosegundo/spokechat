//
//  Chat.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 19/07/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import Foundation

struct Chat: Identifiable {
    let id: UUID

    let receiver: ChatContact
    let messages: [ ChatMessage ]
    
    enum Change {
        case receiver(ChatContact)
        case messages([ ChatMessage ])
    }
    
    init(_ changes:   Change...) { self.init(changes)           }
    init(_ changes: [ Change ] ) { self = Chat().alter(changes) }

    func alter(_ changes:  Change...) -> Chat { alter(changes) }
    func alter(_ changes: [ Change ]) -> Chat { changes.reduce(self) { $0.alter($1) } }

    private func alter(_ change: Change) -> Chat {
        switch change {
        case .receiver(let receiver): return .init(id, receiver, messages)
        case .messages(let messages): return .init(id, receiver, messages)
        }
    }
    
    private init() {
        self.init(UUID(),ChatContact(handle: ""),[])
    }
    
    private init(_ id: UUID,  _ receiver: ChatContact, _ messages: [ChatMessage]) {
        self.id = id
        self.receiver = receiver
        self.messages = messages
    }
}

enum ChatError: Error, Equatable {
    case undefined(String)
}

extension Chat: Equatable, Hashable { }
