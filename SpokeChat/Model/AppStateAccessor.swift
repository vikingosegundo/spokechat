//
//  AppStateAccessor.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 19/07/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

class AppStateAccessor {
    init(appstateStore: AppStateStore) {
        self.appState = AppState()
        self.appstateStore = appstateStore
    }
    func handle(_ msg: Message) {
        if case Message.appState(.wasUpdated(let state)) = msg {
            appState = state
        }
    }
    
    let appstateStore: AppStateStore
    
    private(set)
    var appState: AppState
}
