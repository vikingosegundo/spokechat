//
//  ViewState.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 07/08/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import Combine

final
class ViewState: ObservableObject  {
    init() {
        contacts = []
        unapprovedContacts = []
        user = ChatUser(handle:"")
        chats = []
    }
    
    @Published var contacts            : [ ChatContact ]
    @Published var unapprovedContacts  : [ ChatContact ] { didSet { showApprovalView = unapprovedContacts.count > 0 } }
    @Published var user                : ChatUser
    @Published var chats               : [ Chat ]
    @Published var showApprovalView    : Bool = false

    func process(appState: AppState) {
        chats       = appState.currentChats
        user        = appState.user ?? ChatUser(handle: "")
        contacts    = appState.chatContacts
        unapprovedContacts = appState.unapproved
        showApprovalView   = appState.unapproved.count > 0
    }
}
