//
//  ChatCredentials.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 19/07/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

struct ChatCredentials: CustomStringConvertible {
    let username: String
    let password: String
    
    var description: String { "ChatCredentials(username: \(username), password: ********)" }
}

extension ChatCredentials: Equatable { }
