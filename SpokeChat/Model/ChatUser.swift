//
//  ChatUser.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 19/07/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

struct ChatUser {
    let handle: String
}

extension ChatUser: Equatable, Hashable {
    static func == (lhs: ChatUser, rhs: ChatUser) -> Bool { lhs.handle == rhs.handle }
}
