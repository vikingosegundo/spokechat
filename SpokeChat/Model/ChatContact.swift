//
//  Subscription.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 27/07/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import Foundation.NSUUID

struct ChatContact: Hashable, Equatable, Identifiable {
    
    init(handle: String) {
        self.init(UUID(), handle, nil)
    }
    
    let id: UUID
    let handle: String
    let nickname: String?
    
    var nicknameOrHandle: String {
        nickname != nil && nickname != ""
            ? nickname!
            : handle
    }
    
    enum Change {
        case nickname(String?)
    }
    func alter(_ changes:  Change...) -> ChatContact { alter(changes) }
    func alter(_ changes: [ Change ]) -> ChatContact { changes.reduce(self) { $0.alter($1) } }

    private func alter(_ change: Change) -> ChatContact {
        switch change {
        case .nickname(let nickname): return ChatContact(id, handle, nickname)
        }
    }
    private init (_ id: UUID, _ handle: String, _ nickname: String?) {
        self.id = id
        self.handle = handle
        self.nickname = nickname
    }
    
    static func == (lhs: ChatContact, rhs: ChatContact) -> Bool { lhs.handle == rhs.handle }

}
