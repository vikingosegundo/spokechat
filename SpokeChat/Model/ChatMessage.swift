//
//  ChatMessage.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 22/07/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import Foundation

struct ChatMessage: Identifiable {
    
    let id: UUID
    let from: ChatContact
    let to: ChatContact
    let body: String
    let wasReadBefore: Bool
    let created: Date
    
    enum Change {
        case wasReadBefore(Bool)
    }
    
    init(from: ChatContact, to: ChatContact, body: String) {
        self.init(UUID(), from, to, body, false, Date())
    }
        
    init(from: ChatContact, to: ChatContact, body: String, created:Date) {
        self.init(UUID(), from, to, body, false, created)
    }
    
    private init(_ id: UUID, _ from: ChatContact, _ to: ChatContact, _ body: String, _ wasReadBefore:Bool, _ created:Date) {
        self.id = id
        self.from = from
        self.to = to
        self.body = body
        self.wasReadBefore = wasReadBefore
        self.created = created
    }
    
    func alter(_ changes: Change...) -> ChatMessage { changes.reduce(self) { $0.alter($1) } }
    
    private func alter(_ change: Change) -> ChatMessage {
        switch change {
        case .wasReadBefore(let wasReadBefore): return ChatMessage(id, from, to, body, wasReadBefore, created)
        }
    }
}

extension ChatMessage: Equatable, Hashable { }
