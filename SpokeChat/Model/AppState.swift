//
//  AppState.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 16/07/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

struct AppState {

    enum Change {
        case user        (  ChatUser     )
        case currentChats([ Chat        ])
        case contacts    ([ ChatContact ])
        case unapproved  ([ ChatContact ])
    }

    let user        :   ChatUser?
    let currentChats: [ Chat        ]
    let chatContacts: [ ChatContact ]
    let unapproved  : [ ChatContact ]

    func alter(_ changes: Change...) -> AppState { changes.reduce(self) { $0.alter($1) } }

    init () {
        self.user = nil
        self.currentChats = []
        self.chatContacts = []
        self.unapproved   = []
    }
    
    // MARK: - private
    private init(_ user:ChatUser?, _ currentChats: [Chat], _ contacts: [ChatContact], _ unapproved: [ChatContact]) {
        self.user = user
        self.currentChats = currentChats
        self.chatContacts = contacts
        self.unapproved = unapproved
    }

    private func alter(_ change:Change) -> AppState {
        switch change {
        case .user         (let user        ): return AppState(user, currentChats, chatContacts, unapproved)
        case .currentChats (let currentChats): return AppState(user, currentChats, chatContacts, unapproved)
        case .contacts     (let chatContacts): return AppState(user, currentChats, chatContacts, unapproved)
        case .unapproved   (let unapproved  ): return AppState(user, currentChats, chatContacts, unapproved)
        }
    }
}

extension AppState: Equatable, Hashable {}
