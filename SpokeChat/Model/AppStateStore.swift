//
//  AppStateStore.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 16/07/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import Foundation

final
class AppStateStore {
    
    init(state: AppState? = nil) {
        self.state = state ?? AppState()
    }
    
    var state: AppState {
        didSet {
            updated?(state)
        }
    }
    
    var error: Error?
    
    var updated: ((AppState) -> ())?
    func handle(msg: Message) {
        DispatchQueue.main.async {
            if case .user    (let msg) = msg { self.handle(msg) }
            if case .chat    (let msg) = msg { self.handle(msg) }
            if case .contact (let msg) = msg { self.handle(msg) }
            if case .appState(let msg) = msg { self.handle(msg) }
        }
    }
}

//MARK: - Message Handling
fileprivate extension AppStateStore {
    
    private func handle(_ msg: Message._User) { /*resetAppState()*/ }
    
    private func handle(_ msg: Message._Contact) {
        switch msg {
        case .request (let msg): handle(msg)
        case .response(let msg): handle(msg)
        case .updated(let contact):  replace(state: state.alter(.contacts(state.chatContacts.filter{ $0.handle != contact.handle } + [contact])))}
    }
    
    private func handle(_ msg: Message._AppState) {
        switch msg {
        case .update(let s): replace(state: s)
        case .wasUpdated(_): break
        case .wasNotUpdated(_): break
        }
    }
    private func handle(_ msg: Message._Contact._Request) {
        if case .addIncomingContact(let c) = msg { process(contact: c)}
    }
    
    private func handle(_ msg: Message._Contact._Response) {
        if case .contactWasAdded          (let c) = msg { process(contact: c)    }
        if case .addContactRequestReceived(let c) = msg { process(unapproved: c) }
    }
    
    private func handle(_ msg: Message._Chat) {
        if case .message     (let msg) = msg { self.handle(msg) }
        if case .response    (let msg) = msg { self.handle(msg) }
        if case .subscription(let msg) = msg { self.handle(msg) }
    }
    
    private func handle(_ msg: Message._Chat._Subscription) {
        switch msg {
        case .response(.wasSaved(let s)): process(contact: s)

        default:
            break
        }
    }
     
    private func handle(_ msg: Message._Chat._Message) {
        if case .send    (let cm) = msg { self.process(chatMessage: cm) }
        if case .received(let cm) = msg { self.process(chatMessage: cm) }
        if case .wasRead (let cm) = msg { self.wasRead(chatMessage: cm) }
    }
    
    private func handle(_ msg: Message._Chat._Response) {
        if case .wasDeleted  (let t   ) = msg { replace(state: t.newState) }
        if case .userLoggedIn(let user) = msg { replace( user: user      ) }
    }
}

//MARK: - State Update
fileprivate extension AppStateStore {

    private func replace(state s: AppState) { state = s                               }
    private func replace(user   : ChatUser) { replace(state:state.alter(.user(user))) }
    private func resetAppState()            { replace(state: AppState())              }

    private func process(contact: ChatContact) {
        replace(state: state.alter(.contacts((Set(state.chatContacts.filter { $0.handle != contact.handle } + [contact])).sorted { $0.handle < $1.handle }), .unapproved(state.unapproved.filter { $0.handle != contact.handle })))
    }
    private func process(unapproved contact: ChatContact) {
        replace(state: state.alter(.unapproved(state.unapproved + [contact])))
    }
    
    private func process(chatMessage cm: ChatMessage) {
        let chat = state.chatFor(chatMessage: cm)
        replace (
            state:
                chat == nil
                    ? state.stateByAddingNewChat(with: cm)
                    : state.stateByAdding(chatMessage: cm, toExisting: chat!)
        )
    }
    
    private func wasRead(chatMessage cm:ChatMessage) {
        guard !cm.wasReadBefore else { return }
        let chat = state.chatFor(chatMessage: cm)
        if let chat = chat {
            replace(state: state.stateByFlaggingRead(chatMessage: cm, in: chat))
        }
    }
}

fileprivate extension AppState {
    func stateByAddingNewChat(with cm: ChatMessage) -> AppState {
        let newChat = Chat(.receiver((cm.from.handle != (user ?? ChatUser(handle: "")).handle ? cm.from : cm.to)), .messages([ cm ]))
        return alter(
            .currentChats( currentChats + [ newChat ].filter { _ in user != nil }
            )
        )
    }
    
    func stateByAdding (chatMessage cm: ChatMessage, toExisting chat: Chat) -> AppState {
        let updatedChat = chat.alter(.messages( Set(chat.messages + [ cm ]).sorted { $0.created < $1.created}))
        return alter(
            .currentChats(currentChats.filter { $0.id != chat.id } + [  updatedChat ].filter({ _ in user != nil }))
        )
    }
    
    func stateByFlaggingRead(chatMessage cm: ChatMessage, in chat: Chat) -> AppState {
        let updatedChat = chat.alter(.messages(Set(chat.messages.filter { $0 != cm } + [ cm.alter(.wasReadBefore(true)) ]).sorted { $0.created < $1.created } ))
        return alter(
            .currentChats(currentChats.filter { $0.id != chat.id } + [ updatedChat ].filter({ _ in user != nil }))
        )
    }
}

//MARK: - Tools
fileprivate extension AppState {
    func chatFor(chatMessage cm: ChatMessage) -> Chat? { currentChats.filter { ($0.receiver == cm.from || $0.receiver == cm.to)  }.first }    
}
