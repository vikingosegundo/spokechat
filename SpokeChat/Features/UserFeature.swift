//
//  UserFeature.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 09/07/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

func createUserFeature(appStateAccessor: AppStateAccessor,
                                 output: @escaping Output) -> Input
{    
    let fetchCurrentUser = FetchCurrentUserUseCase(appStateAccessor: appStateAccessor) { (response) in
        switch response {
        case .currentUser(let user): output(.user(.response(.currentUser(user))))
        case .noCurrentUser: output(.user(.response(.noCurrentUser)))
        }
    }
    
    return { msg in
        if case .user(.request(.currentUser)) = msg { fetchCurrentUser.request(.fetchCurrentUser) }
    }
}
