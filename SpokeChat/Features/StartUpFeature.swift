//
//  StartUpFeature.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 09/07/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

func createStartUpFeature(output: @escaping Output) -> Input {
    return { msg in
        if case .appStart(.initialize)     = msg { output(.appStart(.wasInitialized)) }
        if case .appStart(.wasInitialized) = msg { output(.user(.request(.currentUser)))}
    }
}

