//
//  ContactFeature.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 28/07/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

func createContactFeature(
        addContactGateway: AddContactGateway,
    replaceContactGateway: ReplacingContactGateway,
            appstateStore: AppStateStore,
                   output: @escaping Output) -> Input
{
    let contactAdder = AddContactUseCase(appstateStore: appstateStore, addContactGateway: addContactGateway) { response in
        switch response {
        case .contactWasAdded    (let id                ): output(.contact(.response(.contactWasAdded(id))))
        case .contactWasNotAdded (let contact, let error): output(.contact(.response(.addingContactFailed((contact: contact, error: error)))))
        case .addContactRequestReceived(let contact     ): output(.contact(.response(.addContactRequestReceived(contact))))
        case .contactWasUpdated  (let contact           ): output(.contact(.updated(contact)))
        }
    }
    let replaceContact = ReplaceContactUseCase(replaceContactGateway: replaceContactGateway) { response in
        switch response {
        case .wasReplaced(   old: _, with: _,    in: let state): output(.appState(.update(state)))
        case .wasNotReplaced(old: _, with: _, error: let error): output(.appState(.wasNotUpdated(error)))
        }
    }
    return { msg in
        if case Message.contact(.request(.addContact(let id)))         = msg { contactAdder.request(.addContact(id)) }
        if case Message.contact(.request(.addIncomingContact(let id))) = msg { contactAdder.request(.addContact(id)) }
        if case Message.contact(.request(.replace(let oldNew)))        = msg { replaceContact.request(.replace(old: oldNew.contact, with: oldNew.with))
        }
    }
}
