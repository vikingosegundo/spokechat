//
//  StateFeature.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 28/07/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

func createStateFeature(appstateStore: AppStateStore, output: @escaping Output) -> Input {
    appstateStore.updated = {
        output(.appState(.wasUpdated($0)))
    }
    
    return { msg in
        appstateStore.handle(msg: msg)
    }
}
