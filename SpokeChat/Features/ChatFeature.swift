//
//  ChatFeature.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 09/07/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import Foundation

func createChatFeature(appStateAccessor: AppStateAccessor,
                         messageGateway: MessageGateway,
                           logInGateway: LoginGateway,
                                 output: @escaping Output) -> Input
{
    let logIn = LogInUserUseCase(appStateAccessor: appStateAccessor, logInGateway: logInGateway) { response in
        switch response {
        case .loggedIn(let user)       : output(.chat(.response(.userLoggedIn(user))))
        case .loginFailed(let username): output(.chat(.response(.loginFailed(ChatError.undefined(username)))))
        }
    }

    let messageSender = MessageSenderUseCase(appStateAccessor: appStateAccessor, messageGateway: messageGateway) { (response) in
        switch response {
        case .messageSent(let cm): output(.chat(.message(.wasSend(cm))))
        }
    }
    
    let messageReceiver = MessageReceivingUseCase(messageGateway: messageGateway) { (response) in
        switch response {
        case .received(let cm): output(.chat(.message(.received(cm))))
        }
    }
    
    let chatRemoval = ChatRemovalUseCase(appStateAccessor: appStateAccessor) { (response) in
        switch response {
        case .wasDeleted(let chat, let newState): output(.chat(.response(.wasDeleted((chat: chat, newState: newState)))))
        }
    }
    
    let saveSubscription = SaveSubscriptionUseCase(diskGateway: SubscriptionDiskGateway(fileManager: FileManager.default)) { (response) in
        switch response {
        case .wasSaved(let s): output(.chat(.subscription(.response(.wasSaved(s)))))
        case .failedSaving(let s):  output(.chat(.subscription(.response(.failedSaving(s)))))
        }
    }
    
    return { msg in
        if case .chat(.request(.login(let creds)))                = msg {            logIn.request(.login(creds)     ) }
        if case .chat(.message( .send(let   msg)))                = msg {    messageSender.request(.sendMessage(msg) ) }
        if case .chat(.request(.delete(let chat)))                = msg {      chatRemoval.request(.delete(chat)     ) }
        if case .chat(.subscription(.response(.received(let s)))) = msg { saveSubscription.request(.save(s)          ) }
        if case .noop                                             = msg {  messageReceiver.request(.noop             ) }
    }
}
