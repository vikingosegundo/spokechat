//
//  MessageSendingUseCase.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 19/07/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

struct MessageSenderUseCase: UseCase  {
    
    enum Request {
        case sendMessage(ChatMessage)
    }
    enum Response {
        case messageSent(ChatMessage)
    }
    
    init(
        appStateAccessor: AppStateAccessor,
          messageGateway: MessageGateway,
               responder: @escaping (Response) -> Void
    ) {
        self.appStateAccessor = appStateAccessor
        self.respond = responder
        self.messageGateway = messageGateway
    }
    
    func request(_ request: Request) {
        switch request {
        case .sendMessage(let data): messageGateway.sendMessage(data); respond(.messageSent(data))
        }
    }

    typealias RequestType = Request
    typealias ResponseType = Response
    
    private let respond: ((Response) -> ())
    private let appStateAccessor: AppStateAccessor
    private let messageGateway: MessageGateway
}
