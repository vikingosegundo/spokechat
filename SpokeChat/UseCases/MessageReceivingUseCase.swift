//
//  MessageReceivingUseCase.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 04/08/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

struct MessageReceivingUseCase: UseCase {
    enum Request {
        case noop
    }
    enum Response {
        case received(ChatMessage)
    }
    
    init(messageGateway: MessageGateway, responder: @escaping (Response) -> Void) {
        messageGateway.chatMessageReceived = { cm in responder(.received(cm))}
        self.messageGateway = messageGateway
    }
    
    func request(_ request: Request) {
        
    }
    
    typealias RequestType = Request
    typealias ResponseType = Response

    private let messageGateway: MessageGateway
}
