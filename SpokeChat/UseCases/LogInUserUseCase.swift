//
//  LogInUserUseCase.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 09/07/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

class LogInUserUseCase: UseCase {
    
    enum Request {
        case login(ChatCredentials)
    }
    enum Response {
        case loggedIn(ChatUser)
        case loginFailed(String)
    }
    
    init(
        appStateAccessor: AppStateAccessor,
             logInGateway: LoginGateway,
                responder: @escaping (Response) -> Void
    ) {
        self.appStateAccessor = appStateAccessor
        self.logInGateway = logInGateway
        self.respond = responder
    }
    
    typealias RequestType = Request
    typealias ResponseType = Response
    
    private let appStateAccessor: AppStateAccessor
    private let logInGateway: LoginGateway
    private let respond: ((Response) -> ())
    private var username: String?
    
    func request(_ request: Request) {
        switch request {
        case .login(let creds):
            self.username = creds.username
            logInGateway.loginSuccessfull = {
                self.respond(.loggedIn(ChatUser(handle: creds.username)))
            }
            logInGateway.logIn(credentials: creds)
        }
    }
}
