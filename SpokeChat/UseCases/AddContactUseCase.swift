//
//  AddContactUseCase.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 06/08/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

class AddContactUseCase: UseCase {
    
    init(appstateStore: AppStateStore, addContactGateway: AddContactGateway, responder: @escaping (Response) -> Void) {
        self.respond           = responder
        self.addContactGateway = addContactGateway
        self.appstateStore     = appstateStore
        
        self.addContactGateway.contactRequestReceived = {
            self.respond(.addContactRequestReceived($0))
        }
        self.addContactGateway.contactUpdated = {
            self.respond(.contactWasUpdated($0))
        }
    }
    
    func request(_ request: Request) {
        switch request {
        case .addContact(let contact):
            addContactGateway.add(contact: contact) {
                switch $0 {
                case .success(let contact): self.respond(.contactWasAdded(contact))
                case .failure(let error)  : self.respond(.contactWasNotAdded(contact, error))
                }
            }
        }
    }
    
    enum Request {
        case addContact(ChatContact)
    }
    
    enum Response {
        case contactWasAdded(ChatContact)
        case contactWasNotAdded(ChatContact, AddContactError)
        case addContactRequestReceived(ChatContact)
        case contactWasUpdated(ChatContact)
    }
    
    typealias RequestType = Request
    typealias ResponseType = Response
    
    private let respond: (Response) -> Void
    private let addContactGateway:AddContactGateway
    private let appstateStore: AppStateStore
}
