//
//  ReplaceContactUseCase.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 09/08/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

struct ReplaceContactUseCase: UseCase {
    
    init(replaceContactGateway: ReplacingContactGateway, responder: @escaping (Response) -> Void) {
        self.replaceContactGateway = replaceContactGateway
        self.respond = responder
    }
    
    func request(_ request: Request) {
        switch request {
        case .replace(old: let old, with: let new):
            replaceContactGateway.replace(old: old, new: new) {
                switch $0 {
                case .success(let s): respond(.wasReplaced(old: old, with: new, in: s))
                case .failure(let e): respond(.wasNotReplaced(old: old, with: new, error: e))
                }
            }
        }
    }

    enum Request {
        case replace(old: ChatContact, with: ChatContact)
    }
    enum Response {
        case wasReplaced(old: ChatContact, with: ChatContact, in: AppState)
        case wasNotReplaced(old: ChatContact, with: ChatContact, error: Error)
    }

    typealias RequestType = Request
    typealias ResponseType = Response
    
    private let replaceContactGateway: ReplacingContactGateway
    private let respond: ((Response) -> ())
}
