//
//  ChatRemovalUseCase.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 23/07/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

struct ChatRemovalUseCase: UseCase {
    
    enum Request {
        case delete(Chat)
    }
    enum Response {
        case wasDeleted(Chat, AppState)
    }
    
    init(
        appStateAccessor: AppStateAccessor,
               responder: @escaping (Response) -> Void
    ) {
        self.appStateAccessor = appStateAccessor
        self.respond = responder
    }

    
    func request(_ request: Request) {
        switch request {
        case .delete(let chat):
            let state = appStateAccessor.appState.alter(.currentChats(appStateAccessor.appState.currentChats.filter{ $0 != chat }))
            respond(.wasDeleted(chat, state))
        }
    }
    
    typealias RequestType = Request
    typealias ResponseType = Response
    
    private let appStateAccessor: AppStateAccessor
    private let respond: ((Response) -> ())
}
