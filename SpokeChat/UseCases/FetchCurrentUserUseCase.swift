//
//  FetchCurrentUserUseCase.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 19/07/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

struct FetchCurrentUserUseCase: UseCase {
    
    enum Request {
        case fetchCurrentUser
    }
    enum Response {
        case noCurrentUser
        case currentUser(ChatUser)
    }
    
    init(
        appStateAccessor: AppStateAccessor,
               responder: @escaping (Response) -> Void
    ) {
        self.appStateAccessor = appStateAccessor
        self.respond = responder
    }

    func request(_ request: Request) {
        switch request {
        case .fetchCurrentUser:
            let user = fetch()
            if let user = user {
                respond(.currentUser(user))
            } else {
                respond(.noCurrentUser)
            }
        }
    }
    
    typealias RequestType = Request
    typealias ResponseType = Response
    
    private let respond: ((Response) -> ())
    private let appStateAccessor: AppStateAccessor
    
    private func fetch() -> ChatUser? {
        appStateAccessor.appState.user
    }
}
