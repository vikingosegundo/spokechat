//
//  SaveSubscriptionUseCase.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 06/08/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

class SaveSubscriptionUseCase: UseCase {
    
    init(
        diskGateway: SubscriptionDiskGateway,
        responder: @escaping (Response) -> ()
    ) {
        self.diskGateway = diskGateway
        self.respond = responder
    }
    
    func request(_ request: Request) {
        switch request {
        case .save(let subscription):
            diskGateway.save(subscription: subscription) { result in
                switch result {
                case .success(let s): self.respond(.wasSaved(s))
                case .failure(let e):
                    switch e {
                    case .savingFailed(let s): self.respond(.failedSaving(s))
                    }
                }
            }
        }
    }
    
    enum Request {
        case save(ChatContact)
    }
    enum Response {
        case wasSaved(ChatContact)
        case failedSaving(ChatContact)
    }
    
    typealias RequestType = Request
    typealias ResponseType = Response
    
    private let respond: (Response) -> ()
    private let diskGateway: SubscriptionDiskGateway
}
