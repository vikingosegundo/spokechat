//
//  createApp.swift
//  SpokeChat
//
//  Copyright © 2020 Manuel. All rights reserved.
//

typealias  Input = (Message) -> ()
typealias Output = (Message) -> ()

typealias Gateways = (
           login: LoginGateway,
       messaging: (MessageGateway),
      addContact: AddContactGateway,
    replacingContact: ReplacingContactGateway
)

func createAppCore(
                       receivers: [Input],
                        gateways: Gateways,
                appStateAccessor: AppStateAccessor,
                     rootHandler: @escaping Output) -> Input
{
    let features: [Input] = [
        createContactFeature(addContactGateway: gateways.addContact, replaceContactGateway:gateways.replacingContact , appstateStore: appStateAccessor.appstateStore, output: rootHandler),
        createStateFeature  (appstateStore: appStateAccessor.appstateStore, output: rootHandler),
        createStartUpFeature(output: rootHandler),
        createUserFeature   (appStateAccessor: appStateAccessor, output: rootHandler),
        createChatFeature   (appStateAccessor: appStateAccessor, messageGateway: gateways.messaging, logInGateway: gateways.login, output: rootHandler)
    ]
    
    return { msg in
        print(msg)
        (receivers + [appStateAccessor.handle(_:)] + features).forEach { $0(msg) }
    }
}
