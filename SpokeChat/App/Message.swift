//
//  Message.swift
//  SpokeChat
//
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

enum Message {
    
    case appStart(_AppStart)
    case appState(_AppState)
    case chat(_Chat)
    case user(_User)
    case contact(_Contact)
    case noop
    
    enum _AppState {
        case update(AppState)
        case wasUpdated(AppState)
        case wasNotUpdated(Error)
    }
    
    enum _AppStart {
        case initialize
        case wasInitialized
    }
    
    enum _Chat {
        case request(_Request)
        case response(_Response)
        case message(_Message)
        case subscription(_Subscription)
        
        enum _Subscription {
            case response(_Response)
            case request(_Request)
            
            enum _Request {
                case send(ChatContact)
            }
            enum _Response {
                case received(ChatContact)
                case wasSaved(ChatContact)
                case failedSaving(ChatContact)
            }
        }
        
        enum _Message {
            case received(ChatMessage)
            case send(ChatMessage)
            case wasSend(ChatMessage)
            case failedSending(ChatMessage)
            case wasRead(ChatMessage)
        }

        enum _Request {
            case login(ChatCredentials) // -> (.response(.userLoggedIn(user)) | .response(.loginFailed(error)))
            case all                    // -> .response(.all(chats))
            case delete(Chat)

        }
        
        enum _Response {
            case userLoggedIn(ChatUser)
            case loginFailed(ChatError)
            case all([Chat])
            case wasDeleted((chat:Chat, newState:AppState))
        }
    }
    
    enum _User {
        case request(_Request)
        case response(_Response)
        
        enum _Request {
            case currentUser // -> (.response(.currentUser(user)) | .response(.noCurrentUser))
        }
        
        enum _Response {
            case currentUser(ChatUser)
            case noCurrentUser
        }
    }
    
    enum _Contact {
        
        case request(_Request)
        case response(_Response)
        case updated(ChatContact)
        
        enum _Request {
            case replace((contact: ChatContact, with: ChatContact))
            case addContact(ChatContact)
            case addIncomingContact(ChatContact)
        }
        
        enum _Response {
            case contactWasAdded(ChatContact)
            case addingContactFailed((contact: ChatContact, error: AddContactError))
            case addContactRequestReceived(ChatContact)
        }
    }
}
