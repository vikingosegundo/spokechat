//
//  SpokeChatApp.swift
//  SpokeChat
//
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import SwiftUI

@main
final
class SpokeChatApp: App {
    init() { }
    
    var body: some Scene {
        WindowGroup {
            view
        }
    }
    
    private let xmppStack = XMPPStack()
    private let appstateStore = AppStateStore()
    private lazy var appStateAccessor: AppStateAccessor = AppStateAccessor(appstateStore: appstateStore)
    
    private lazy var gateways: Gateways = (
                   login: XMPPLogInGateway         (xmppStack: xmppStack),
               messaging: XMPPMessageGateway       (xmppStack: xmppStack),
              addContact: XMMPAddContactGateway    (xmppStack: xmppStack),
        replacingContact: XMPPReplaceContactGateway(xmppStack: xmppStack, appStateAccessor:appStateAccessor)
    )
    
    private lazy var rootHandler: (Message) -> () = createAppCore(
            receivers: [ handle(msg: ) ],
             gateways: gateways,
        appStateAccessor: appStateAccessor,
          rootHandler: { self.rootHandler($0) }
    )
    
    private lazy var view = ContentView(
          viewState: ViewState(),
        rootHandler: rootHandler
    )
    
    private func handle(msg: Message) {
        if case .appState(.wasUpdated(let appstate)) = msg {
            view.viewState.process(appState: appstate)
        }
    }
}
