//
//  XMPPStack.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 19/07/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import Foundation
import TigaseSwift


protocol XmppServiceEventHandler: EventHandler {
    var events: [Event] { get }
}

class XMPPStack: EventHandler {
    
    init() {
        client = XMPPClient()
        registerModules()
        connectEventBus()
        registerEventHandlers(client)
    }
    
    let client: XMPPClient
    var loginCallback: (() -> ())?
    var chatMessageReceived: ((ChatMessage) -> ())?
    var subscripionReceived: ((ChatContact) -> ())?
    
    func add(eventHandler: XmppServiceEventHandler) {
        eventHandlers.append(eventHandler)
        registerEventHandlers(client)
    }
    
    func handle(event: Event) {
        switch (event) {
        case is SessionEstablishmentModule.SessionEstablishmentSuccessEvent:
            sessionEstablished()
        case is SocketConnector.DisconnectedEvent:
            break
        case let cpc as PresenceModule.ContactPresenceChanged:
            contactPresenceChanged(cpc)
        default:
            //unsupported event
            break
        }
    }
    
    private lazy var eventHandlers: [XmppServiceEventHandler] = [
        msgEventHandler
    ]
    
    private lazy var msgEventHandler: MessageEventHandler =
        MessageEventHandler(messageEventReceived: { (event) in
                if let chatMesssage = transformEventToChatMessage(event) {
                    self.chatMessageReceived?(chatMesssage)
            }
        }
    )
    
    private
    func registerModules() {
        _ = client.modulesManager.register(AuthModule())
        _ = client.modulesManager.register(StreamFeaturesModule())
        _ = client.modulesManager.register(SaslModule())
        _ = client.modulesManager.register(ResourceBinderModule())
        _ = client.modulesManager.register(SessionEstablishmentModule())
        _ = client.modulesManager.register(PresenceModule())
        _ = client.modulesManager.register(MessageModule())
        _ = client.modulesManager.register(RosterModule())
    }

    private
    func connectEventBus() {
        client.eventBus.register(handler: self, for: SessionEstablishmentModule.SessionEstablishmentSuccessEvent.TYPE)
        client.eventBus.register(handler: self, for: SocketConnector.ConnectedEvent.TYPE)
        client.eventBus.register(handler: self, for: SocketConnector.DisconnectedEvent.TYPE)
        client.eventBus.register(handler: self, for: PresenceModule.ContactPresenceChanged.TYPE, MessageModule.MessageReceivedEvent.TYPE)
        client.eventBus.register(handler: self, for: DiscoveryModule.ServerFeaturesReceivedEvent.TYPE, AuthModule.AuthFailedEvent.TYPE, StreamManagementModule.ResumedEvent.TYPE)
    }
    
    private
    func sessionEstablished() { }
    
    private
    func contactPresenceChanged(_ cpc: PresenceModule.ContactPresenceChanged) { }
    
    private
    func registerEventHandlers(_ client:XMPPClient) {
        for handler in self.eventHandlers {
            client.eventBus.register(handler: handler, for: handler.events)
        }
    }
}

// MARK: - Event Handler
extension XMPPStack {
    private class PresenceRosterEventHandler: XmppServiceEventHandler {
        fileprivate
        var itemUpdatedEventReceived: (RosterModule.ItemUpdatedEvent) -> ()
        var subscribeRequestEventReceived: (PresenceModule.SubscribeRequestEvent) -> ()
        let events: [Event] = [RosterModule.ItemUpdatedEvent.TYPE,PresenceModule.BeforePresenceSendEvent.TYPE, PresenceModule.SubscribeRequestEvent.TYPE];
            
        init(itemUpdatedEventReceived: @escaping (RosterModule.ItemUpdatedEvent) -> (),
             subscribeRequestEventReceived: @escaping (PresenceModule.SubscribeRequestEvent) -> ())
        {
            self.itemUpdatedEventReceived = itemUpdatedEventReceived
            self.subscribeRequestEventReceived = subscribeRequestEventReceived
        }
        
        func handle(event: Event) {
            switch event {
            case let e as PresenceModule.SubscribeRequestEvent:
                subscribeRequestEventReceived(e)
            case let e as RosterModule.ItemUpdatedEvent:
                itemUpdatedEventReceived(e)
            default:
                break;
            }
        }
    }
    
    private class MessageEventHandler:XmppServiceEventHandler {
        fileprivate
        var messageEventReceived: (MessageModule.MessageReceivedEvent) -> ()

        init(messageEventReceived: @escaping (MessageModule.MessageReceivedEvent) -> ()) {
            self.messageEventReceived = messageEventReceived
        }
        
        func handle(event: Event) {
            switch event {
            case let e as MessageModule.MessageReceivedEvent:
                messageEventReceived(e)
            default:
                break
            }
        }

        let events: [Event] = [MessageModule.MessageReceivedEvent.TYPE, MessageDeliveryReceiptsModule.ReceiptEvent.TYPE, MessageCarbonsModule.CarbonReceivedEvent.TYPE, DiscoveryModule.AccountFeaturesReceivedEvent.TYPE, DiscoveryModule.ServerFeaturesReceivedEvent.TYPE, MessageArchiveManagementModule.ArchivedMessageReceivedEvent.TYPE, SessionEstablishmentModule.SessionEstablishmentSuccessEvent.TYPE, StreamManagementModule.ResumedEvent.TYPE /*, OMEMOModule.AvailabilityChangedEvent.TYPE'*/]
    }
}

// MARK: - Tools
fileprivate
func transformEventToChatMessage(_ event: MessageModule.MessageReceivedEvent) -> ChatMessage? {
    if
        let to = event.message.to,
        let from = event.message.from
    {
        return ChatMessage(from: ChatContact(handle: String(from.stringValue.split(separator: "/").first!)), to: ChatContact(handle:String(to.stringValue.split(separator: "/").first!)), body: event.message.body ?? "no message body")
    }
    return nil
}
