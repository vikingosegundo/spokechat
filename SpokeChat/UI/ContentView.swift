//
//  ContentView.swift
//  SpokeChat
//
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import Combine
import SwiftUI
import BottomBar_SwiftUI

struct ContentView: View {
    
    init(viewState: ViewState, rootHandler: @escaping (Message) -> ()) {
        self.viewState = viewState
        self.rootHandler = rootHandler
    }
    
    var body: some View {
            if self.viewState.user.handle == "" {
            LoginView(rootHandler: rootHandler)
                .padding(.horizontal)
        } else {
            NavigationView {
                VStack {
                           if (selectedTab == 0) { settingsView        (viewState: viewState, rootHandler: rootHandler)
                    } else if (selectedTab == 1) { subscriptionListView(viewState: viewState, rootHandler: rootHandler)
                    } else if (selectedTab == 2) { chatListView        (viewState: viewState, rootHandler: rootHandler)
                    }
                    BottomBar(selectedIndex: $selectedTab, items: items)
                }
            }
            .sheet(isPresented: self.$viewState.showApprovalView) {
                Text("approve")
                ForEach(viewState.unapprovedContacts) { contact in
                    VStack {
                        Text(contact.handle)
                        Button(action: { self.add(contact: contact) }) { Text("Add contact")}.padding()
                    }
                }
            }
        }
    }
    
    @State private var selectedTab = 2
    @State private var showApprovalView: Bool = false
    
    @ObservedObject private(set) var viewState: ViewState
    private let rootHandler: (Message) -> ()
    
    private let numTabs = 3
    private let minDragTranslationForSwipe: CGFloat = 30
    private var items: [BottomBarItem] {
        [
            BottomBarItem(icon: "gear",                              title: "Settings",                             color: .purple),
            BottomBarItem(icon: "person.2.fill",                     title: "Contacts",                             color: .pink  ),
            BottomBarItem(icon: "bubble.left.and.bubble.right.fill", title: chatListTabTitle(viewState: viewState), color: .orange),
        ]
    }

    private func add(contact: ChatContact) {
        rootHandler(.contact(.request(.addIncomingContact(contact))))
    }
    private func handleSwipe(translation: CGFloat) {
        if translation > minDragTranslationForSwipe && selectedTab > 0 {
            selectedTab -= 1
        } else  if translation < -minDragTranslationForSwipe && selectedTab < numTabs-1 {
            selectedTab += 1
        }
    }
}


// MARK: - Tabbar
extension ContentView {
    private func settingsView(viewState: ViewState, rootHandler: @escaping (Message) -> ()) -> some View {
        SettingsView(viewState: viewState, rootHandler: rootHandler).tag(0).highPriorityGesture(DragGesture().onEnded {
            self.handleSwipe(translation: $0.translation.width)
        })
    }
    
    private func subscriptionListView(viewState: ViewState, rootHandler: @escaping (Message) -> ()) -> some View {
        SubscriptionListView(viewState: viewState, rootHandler: rootHandler).tag(1).highPriorityGesture(DragGesture().onEnded {
            self.handleSwipe(translation: $0.translation.width)
        })
    }
    
    private func chatListView(viewState: ViewState, rootHandler: @escaping (Message) -> ()) -> some View {
        ChatListView(viewState:viewState, rootHandler: rootHandler).tag(2).highPriorityGesture(DragGesture().onEnded {
            self.handleSwipe(translation: $0.translation.width)
        })
    }
}


// MARK: - Previews
struct ContentView_Previews: PreviewProvider {
    
    static var previews: some View {
        return Group {
            ContentView(viewState: viewState, rootHandler: { _ in })
        }
    }
    private static var viewState: ViewState {
        let vs = ViewState()
        vs.process(appState: AppState().alter(.user(ChatUser(handle: "manuel@spokechat.ch"))))
        return vs
    }
}

// MARK: - Tools
fileprivate
func chatListTabTitle(viewState: ViewState) -> String {
    if viewState.chats.filter({ unreadMessageIn(chat: $0) > 0 }).count == 1 { return "\(viewState.chats.filter { unreadMessageIn(chat: $0) > 0 }.count) new Chat" }
    if viewState.chats.filter({ unreadMessageIn(chat: $0) > 0 }).count >  1 { return "\(viewState.chats.filter { unreadMessageIn(chat: $0) > 0 }.count) new Chats" }
    return "Chats"
}
