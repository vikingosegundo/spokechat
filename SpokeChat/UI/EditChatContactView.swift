//
//  EditChatContactView.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 09/08/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import SwiftUI

struct EditChatContactView: View {
    @Binding var contactToEdit: ChatContact?
    let rootHandler: (Message) -> ()

    @State var newNickName: String = ""
    @Environment(\.presentationMode) var presentationMode
    var body: some View {
        VStack {
            HStack {
                Text(contactToEdit?.handle ?? "")
            }
            HStack {
                Text("Nickname")
                TextField(contactToEdit?.nickname ?? "Enter nickname", text: self.$newNickName).padding()
            }
            HStack {
                Button(action: { rootHandler(.contact(.request(.replace((contact: contactToEdit!, with: contactToEdit!.alter(.nickname(newNickName)))))));self.presentationMode.wrappedValue.dismiss()}) {
                    Text("Save")
                }
            }
        }
    }
}

