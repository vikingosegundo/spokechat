//
//  LoginView.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 18/07/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import SwiftUI

struct LoginView: View {
        
    @State var username: String = "manuel@spokechat.ch"
    @State var password: String = "passw0rd"
    
    let rootHandler: (Message) -> ()
    
    var body: some View {
        VStack {
            HStack {
                Text("Username:")
                TextField("Username", text: $username)
            }
            
            HStack {
                Text("Password:")
                SecureField("Password", text: $password)
            }
            
            Button(action:login) { Text("Login") }
                .disabled(!enabledLoginButton)
        }
    }
    
    private var enabledLoginButton: Bool {
            usernameFormatChecker(username)
         && passwordFormatChecker(password)
    }
    
    private func login() {
        if enabledLoginButton { rootHandler(.chat(.request(.login(ChatCredentials(username: username, password: password)))))  }
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            LoginView(rootHandler: { _ in})
        }
    }
}
