//
//  ChatListItemView.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 24/07/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import SwiftUI

struct ChatListItemView: View {
    let chat: Chat
    let rootHandler: (Message) -> ()
    let showLastChat: Bool

    var body: some View {
        return HStack {
            Image(systemName: "person")
            VStack{
                Text("\(JIDToName(chat.receiver.nicknameOrHandle))" + (unreadMessageIn(chat:chat) > 0 ? " (\(unreadMessageIn(chat:chat)))" : ""))
                    .contextMenu {
                        Button(action: { self.rootHandler(.chat(.request(.delete(self.chat)))) }) { Text("Close Chat") }
                        Button(action: {     UIPasteboard.general.string = self.chat.receiver.handle }) { Text("Copy Username") }
                    }
                if(showLastChat) {
                    Text(chat.messages.isEmpty ? "" : "\(chat.messages[chat.messages.count - 1].body)")
                }
            }
        }
    }
}
