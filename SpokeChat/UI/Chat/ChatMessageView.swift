//
//  ChatMessageView.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 22/07/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import SwiftUI

fileprivate
struct MessageTail: Shape {
    
    let isMine: Bool
    
    func path(in rect: CGRect) -> Path { Path(chatBubblePath(for: rect).cgPath) }
    
    private func chatBubblePath(for rect: CGRect) -> UIBezierPath { UIBezierPath(roundedRect: rect, byRoundingCorners: [.topLeft,.topRight,isMine ? .bottomLeft : .bottomRight], cornerRadii: CGSize(width:20, height: 20))}
}

struct ChatMessageView: View {
    let chatWith   : ChatContact
    let chatMessage: ChatMessage
    let rootHandler: (Message) -> ()
    
    var body: some View {
        Group {
                HStack {
                    if chatMessage.from != chatWith { Spacer() }
                    Text(chatMessage.body)
                        .foregroundColor(chatMessage.from == chatWith ? Color.white : Color.black)
                        .padding(11)
                        .background(Color(chatMessage.from == chatWith ? UIColor.gray : UIColor.white))
                        .clipShape(MessageTail(isMine: true))
                        .contextMenu{ Button(action: { UIPasteboard.general.string = chatMessage.body }) { Text("Copy Text") }}
                    Text(getTime(chatMessage.created))
                    if chatMessage.from == chatWith { Spacer() }
                }
        }.onAppear(perform: messageWasRead)
    }
    
    private func messageWasRead() {
        self.rootHandler(.chat(.message(.wasRead(self.chatMessage))))
    }
}

struct ChatMessageView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            List {
                ChatMessageView(chatWith: ChatContact(handle:"m1@spokechat.ch"), chatMessage: ChatMessage(from:ChatContact(handle: "manuel@spokechat.ch"), to:ChatContact(handle: "m1@spokechat.ch"),     body:"Hey Ho! let's Go!"), rootHandler: { _ in})
                ChatMessageView(chatWith: ChatContact(handle:"m1@spokechat.ch"),
                                chatMessage: ChatMessage(
                                       from: ChatContact(handle: "m1@spokechat.ch"),
                                         to: ChatContact(handle:"manuel@spokechat.ch"),
                                       body: blitzkriegBob()
                                ),
                               rootHandler: { _ in})
            }
        }
    }
}

fileprivate func blitzkriegBob() -> String {
"""
They're forming in a straight line
They're goin through a tight wind
The kids are losing their minds

Blitzkreig Bop

They're piling in the backseat
They're generation steam heat
Pulsating to the back seat

Blitzkreig Bop

Hey Ho Lets Go

Shoot em' in the back now
What they want, I don't know
They're all reved up and ready to go
"""// if you made it here, take a break and listen to: https://www.youtube.com/watch?v=skdE0KAFCEA
}

