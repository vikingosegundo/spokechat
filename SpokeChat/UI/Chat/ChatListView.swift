//
//  ChatListView.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 22/07/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import SwiftUI

struct ChatListView: View {
    
    @ObservedObject var viewState: ViewState
    
    @State var chatToClose: Chat?
    
    private let rootHandler: (Message) -> ()
    
    
    init(viewState: ViewState, rootHandler: @escaping (Message) -> ()) {
        self.viewState = viewState
        self.rootHandler = rootHandler
    }
    
    var body: some View {
        List {
            if (viewState.chats.count > 0) {
                ForEach(viewState.chats) {  chat in
                    NavigationLink(
                        destination: ChatView(chat: chat,
                                              user: self.viewState.user,
                                              chatToClose: self.$chatToClose,
                                              rootHandler: self.rootHandler)
                            .onDisappear(perform:self.deleteChat)
                    )
                    {
                        ChatListItemView(chat: chat, rootHandler: self.rootHandler, showLastChat: true)
                    }
                }
                .onDelete(perform:deleteChat)
            } else {
                Text("Start a chat!")
            }
        }
        .navigationBarTitle("Chats")
        .navigationBarItems(trailing: Text(""))
    }
}

//MARK: - Chat Deletion
private
extension ChatListView {
    func deleteChat() -> () {
        if let chat = self.chatToClose {
            self.rootHandler(.chat(.request(.delete(chat))))
        }
    }
    
    func deleteChat(indexSet: IndexSet?) {
        guard let idx = indexSet?.first else { return }
        self.rootHandler(.chat(.request(.delete(viewState.chats[idx]))))
    }
}


// MARK: - Preview
struct ChatListView_Previews: PreviewProvider {
    
    static private func viewState() -> ViewState {
        let viewState = ViewState()
        viewState.process(appState: appState())
        return viewState
    }
    
    static private func appState() -> AppState {
        AppState().alter(
            .user(ChatUser(handle: "manuel@spokechat.ch")),
            .currentChats([
                Chat(.receiver(ChatContact(handle:"m1@spokechat.ch")), .messages( [ ChatMessage(from:ChatContact(handle: "m1@spokechat.ch"), to:ChatContact(handle:"manuel@spokechat.ch"), body:"Hey Ho, let's go!") ])),
                Chat(.receiver(ChatContact(handle:"m2@spokechat.ch")), .messages( [ ChatMessage(from:ChatContact(handle: "m1@spokechat.ch"), to:ChatContact(handle:"manuel@spokechat.ch"), body:"Hey Ho, let's go!").alter(.wasReadBefore(true)) ])),
                Chat(.receiver(ChatContact(handle:"m3@spokechat.ch")), .messages( [ ChatMessage(from:ChatContact(handle: "m1@spokechat.ch"), to:ChatContact(handle:"manuel@spokechat.ch"), body:"Hey Ho, let's go!"), ChatMessage(from:ChatContact(handle: "m1@spokechat.ch"), to:ChatContact(handle:"manuel@spokechat.ch"), body:"Hey Ho, let's go!") ]))
            ])
        )
    }
    
    static var previews: some View {
        return Group {
            ChatListView(viewState: viewState(), rootHandler: { _ in })
        }
    }
}
