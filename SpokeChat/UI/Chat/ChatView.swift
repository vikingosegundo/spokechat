//
//  ChatView.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 22/07/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import SwiftUI

struct ChatView: View {
    let chat: Chat
    let user: ChatUser
    @Binding var chatToClose: Chat?
    @State var contactToEdit: ChatContact? = nil

    let rootHandler: (Message) -> ()
    
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        return
            VStack{
                ScrollView(.vertical){
                    ScrollViewReader { scrollView in
                        LazyVStack(alignment: .leading) {
                            //                            ForEach(chat.messages.reversed()) { cm in
                            ForEach(Array(chat.messages.reversed().enumerated()), id: \.offset) { index, cm in
                                ChatMessageView( chatWith: self.chat.receiver, chatMessage: cm, rootHandler: self.rootHandler ).id(cm.id).transition(AnyTransition.scale)
                                if(chat.messages.indices.contains(index + 1)){
                                    if(Calendar.current.compare(cm.created, to:chat.messages[index + 1].created, toGranularity: .day) != .orderedSame){
                                        Text(getDate(cm.created)).multilineTextAlignment(.center)
                                    }
                                }
                            }
                            .padding(.top, 10.0).rotationEffect(.radians(.pi))
                            .scaleEffect(x: -1, y: 1, anchor: .center)
                        }
                    }
                }
                .rotationEffect(.radians(.pi))
                .scaleEffect(x: -1, y: 1, anchor: .center)
                MessageSendingView(user: user, receiver: chat.receiver, rootHandler: rootHandler).padding(.horizontal)
            }
            .navigationBarItems(
                trailing: HStack { Button(action: closeChat) { Text("Close") } }
            )
            .navigationBarItems(
                leading:
                    Button(action: {self.contactToEdit = self.chat.receiver}){
                        Text(chat.receiver.nicknameOrHandle)
                            .padding(EdgeInsets.init(top: 0, leading: 20, bottom: -5, trailing: 0))
                            .foregroundColor(Color.white)
                    
                }
            ).sheet(item: $contactToEdit) { _ in
                EditChatContactView(contactToEdit: self.$contactToEdit, rootHandler:self.rootHandler)
            }
    }
    
    func closeChat() {
        self.rootHandler(.chat(.request(.delete(self.chat))))
        self.presentationMode.wrappedValue.dismiss()
    }
}

// MARK: - Previews
struct ChatView_Previews: PreviewProvider {
    
    static let chat = Chat(
        .receiver(ChatContact(handle: "manuel@spokechat.ch")),
        .messages( [
            ChatMessage(from: ChatContact(handle: "manuel@spokechat.ch"), to: ChatContact(handle:"m1@spokechat.ch"),      body: "Hey Ho! Let's Go!"),
            ChatMessage(from: ChatContact(handle: "m1@spokechat.ch"),     to: ChatContact(handle:"mmanuel@spokechat.ch"), body: "Hey Ho! Let's Go!")
        ] )
    )
    
    static var previews: some View {
        BindingHolder()
    }
    
    struct BindingHolder: View {
        @State  var chatToClose: Chat? = Chat()
        var body: some View {
            ChatView(chat: chat, user: ChatUser(handle: "manuel@spokechat.ch"), chatToClose: $chatToClose, rootHandler: { _ in })
        }
    }
}
