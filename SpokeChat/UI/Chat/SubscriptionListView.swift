//
//  SubscriptionListView.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 28/07/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import SwiftUI

struct SubscriptionListView: View {
    
    @ObservedObject var viewState: ViewState
    
    @State var chatOpened : Chat?
    @State var chatToClose: Chat?
    @State var showContactInput: Bool = false
    @State var contactToAdd: String = ""
    @State var showingContactAdd: ChatContact? = nil
    @State var contactToEdit: ChatContact? = nil
    private let rootHandler: (Message) -> ()
    
    init(viewState: ViewState, rootHandler: @escaping (Message) -> ()) {
        self.viewState = viewState
        self.rootHandler = rootHandler
    }
    
    var body: some View {
        HStack{
            if(self.viewState.contacts.count > 0) {
            List(self.viewState.contacts) { subscription in
                NavigationLink(
                    destination: ChatView(chat: self.chat(with: subscription),
                                          user: self.viewState.user,
                                          chatToClose: self.$chatToClose,
                                          rootHandler: self.rootHandler)
                )
                {
                    ChatListItemView(chat: self.chat(with: subscription), rootHandler: self.rootHandler , showLastChat: false)
                        .padding()
                }
                .contextMenu {
                    Button(action: { self.contactToEdit = subscription }) { Text("edit contact") }
                }.sheet(item: $contactToEdit) { _ in
                    EditChatContactView(contactToEdit: self.$contactToEdit, rootHandler:self.rootHandler)
                }
            }
        } else {
            List {
                Text("No Contacts")
            }
        }
        
    }.navigationBarItems(
        trailing: Button(action: { self.showingContactAdd = ChatContact(handle: "")}) { Image(systemName: "person.crop.circle.fill.badge.plus") }
    )
    .navigationBarTitle("Contacts")
    .sheet(item: self.$showingContactAdd) { _ in
        TextField("contact to add", text: self.$contactToAdd).padding()
        Button(action: { self.add(contact: ChatContact(handle: self.contactToAdd)) }) { Text("Add contact")}.padding()
    }
}

private func add(contact: ChatContact) {
    rootHandler(.contact(.request(.addContact(contact))))
}

private func chat(with s: ChatContact) -> Chat {
    var chat = viewState.chats.filter {
        $0.receiver == s
    }.first ?? Chat()
    if chat.receiver.handle == "" {
        chat = chat.alter(.receiver(chat.receiver.handle != "" ? chat.receiver : s ))
    }
    return chat
}
}

//MARK: - Previews

struct SubscriptionListView_Previews: PreviewProvider {
    
    static private func viewState() -> ViewState {
        let viewState = ViewState()
        viewState.process(appState: appState())
        return viewState
    }
    
    static private func appState() -> AppState {
        AppState().alter(
            .user(ChatUser(handle: "manuel@spokechat.ch")),
            .contacts([ ChatContact(handle: "m1@spokechat.ch") ])
        )
    }
    
    static var previews: some View {
        SubscriptionListView(viewState: viewState(), rootHandler: { _ in})
    }
}
