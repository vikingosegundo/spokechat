//
//  MessageSendingView.swift
//  SpokeChat
//
//  Created by Manuel Meyer on 18/07/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import SwiftUI

struct MessageSendingView: View {
    
    var user: ChatUser
    
    let receiver: ChatContact
    let rootHandler: (Message) -> ()
    
    @State var message = ""
    
    var body: some View {
        return HStack {
            TextField("Message...", text: $message).frame(minHeight: CGFloat(30))
            
            Button(action: send) {
                Text("Send")
            }
        }.frame(minHeight: CGFloat(10)).padding()
    }
    
    private func send() {
        rootHandler(.chat(.message(.send(ChatMessage(from: ChatContact(handle: user.handle), to: receiver, body: message)))))
        self.message = ""
    }
}

//MARK: - Previews
struct MessageSendingView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            MessageSendingView( user: ChatUser(handle: "manuel"), receiver: ChatContact(handle: "m1@spokechat.ch"), rootHandler: { _ in } )
        }
    }
}
