//
//  SettingsView.swift
//  SpokeChat
//
//
import SwiftUI

struct SettingsView: View {
    
    @ObservedObject var viewState: ViewState
    private let rootHandler: (Message) -> ()
    
    init(viewState: ViewState, rootHandler: @escaping (Message) -> ()) {
        self.viewState = viewState
        self.rootHandler = rootHandler
    }
    
    var body: some View {
        ZStack{
            Form{
                Section{
                    HStack(alignment: .center){
                        Image(systemName: "person")
                            .resizable()
                            .cornerRadius(20)
                            .frame(width: 60, height: 60)
                            .aspectRatio(contentMode: .fill)
                        
                        VStack(alignment: .leading){
                            Text("Chatname")
                                .foregroundColor(Color.blue)
                                .font(Font.system(size:15))
                            
                            Text(self.viewState.user.handle)
                                .foregroundColor(Color.gray)
                                .font(Font.system(size:15))
                                .lineLimit(nil)
                        }
                        .padding(.leading, 5.0)
                    }
                }
                Group(){
                    Section{
                        NavigationLink(destination: Text("Hi There!")){
                            HStack{
                                Image(systemName: "pencil.and.ellipsis.rectangle")
                                    .resizable()
                                    .frame(width: 30, height: 20)
                                    .clipped()
                                    .aspectRatio(contentMode: .fit)
                                Text("Change app password")
                                    .font(Font.system(size:15))
                                
                                
                            }
                        }
                        NavigationLink(destination: Text("Hi There!")){
                            HStack{
                                
                                Image(systemName: "timer")
                                    .resizable()
                                    .frame(width: 20, height: 20)
                                    .clipped()
                                    .aspectRatio(contentMode: .fit)
                                Text("Auto-logout Timer")
                                    .font(Font.system(size:15))
                                    .padding(.leading, 10.0)
                            }
                        }
                    }
                }
            }
            .navigationBarTitle("Settings")
            .navigationBarItems(trailing: Text(""))
        }
    }
}
//MARK: - Previews
struct SettingsView_Previews: PreviewProvider {
    
    static private func viewState() -> ViewState {
        let viewState = ViewState()
        viewState.process(appState: appState())
        return viewState
    }
    
    static private func appState() -> AppState {
        AppState().alter(
            .user(ChatUser(handle: "manuel@spokechat.ch")),
            .currentChats( [
                Chat(.receiver(ChatContact(handle: "m1@spokechat.ch")), .messages( [ ChatMessage(from:ChatContact(handle: "m1@spokechat.ch"), to:ChatContact(handle:"manuel@spokechat.ch"), body:"Hey Ho, let's go!") ])),
                Chat(.receiver(ChatContact(handle: "m2@spokechat.ch")), .messages( [ ChatMessage(from:ChatContact(handle: "m1@spokechat.ch"), to:ChatContact(handle:"manuel@spokechat.ch"), body:"Hey Ho, let's go!").alter(.wasReadBefore(true)) ])),
                Chat(.receiver(ChatContact(handle: "m3@spokechat.ch")), .messages( [ ChatMessage(from:ChatContact(handle: "m1@spokechat.ch"), to:ChatContact(handle:"manuel@spokechat.ch"), body:"Hey Ho, let's go!"), ChatMessage(from:ChatContact(handle: "m1@spokechat.ch"), to:ChatContact(handle:"manuel@spokechat.ch"), body:"Hey Ho, let's go!") ]))
            ] )
        )
    }
    
    static var previews: some View {
        return Group {
            SettingsView(viewState: viewState(), rootHandler: { _ in })
        }
    }
}
