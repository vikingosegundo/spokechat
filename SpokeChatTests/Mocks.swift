//
//  File.swift
//  SpokeChatTests
//
//  Created by Manuel Meyer on 05/08/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import Foundation

@testable import SpokeChat

class LoginGatewayMock: LoginGateway {
    var loginSuccessfull: (() -> ())?
    var shallFail = false
    func logIn(credentials: ChatCredentials) {
        if !shallFail {
            loginSuccessfull?()
        }
    }
}

class MessageSendingGatewayMock: MessageGateway {
    func sendMessage(_ data: ChatMessage) {
        
    }
    
    func receiveMessage(_ data: ChatMessage) {
        chatMessageReceived?(data)
    }
    var chatMessageReceived: ((ChatMessage) -> ())?
}

class AddContactGatewayMock: AddContactGateway {

    var contactUpdated: ((ChatContact) -> ())?
    
    var contactRequestReceived: ((ChatContact) -> ())?
    func add(contact: ChatContact, callback: @escaping (Result<ChatContact, AddContactError>) -> ()) {
        callback(.success(contact))
    }
}

class ReplaceContactGatewayMock: ReplacingContactGateway {
    init(appStateAccessor: AppStateAccessor) {
        self.appStateAccessor = appStateAccessor
    }
    
    func replace(old: ChatContact, new: ChatContact, callback: @escaping (Result<AppState, ReplacingContactError>) -> ()) {
        callback(
            .success(appStateAccessor.appState.alter(.contacts(appStateAccessor.appState.chatContacts.filter{ $0.handle != old.handle} + [new])))
        )
    }
    
    private let appStateAccessor: AppStateAccessor
}

func mockGateways(appStateAccessor: AppStateAccessor) -> Gateways {
    (
           login: LoginGatewayMock(),
       messaging: (MessageSendingGatewayMock()),
      addContact: AddContactGatewayMock(),
replacingContact: ReplaceContactGatewayMock(appStateAccessor: appStateAccessor)
    )
}
