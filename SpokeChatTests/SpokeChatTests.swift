//
//  SpokeChatTests.swift
//  SpokeChatTests
//
//  Created by Manuel Meyer on 05/08/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import Foundation
import Quick
import Nimble

@testable import SpokeChat

class SpokeChatSpec: QuickSpec {
    
    var app: Input! // <-- sut, in case you didnt noticed
    var messageGatewayMock: MessageSendingGatewayMock!
    var loginGatewayMock: LoginGatewayMock!
    var addContactGatewayMock: AddContactGatewayMock!
    var appStateAccessor: AppStateAccessor!
    var appstateStore: AppStateStore! {
        appStateAccessor.appstateStore
    }
    
    override func spec() {
        context("Chat") {
            
            let blitzkriegBob = [
                "Hey Ho",
                "Let´s go",
                "They're forming in a straight line",
                "They're going through a tight wind",
                "The kids are losing their minds",
                "The Blitzkrieg Bop"
            ]
            
            let petcementary = [
                "I dont wanna be burried on a Pet Cementary",
                "I dont wanna live my life again"
            ]
            
            let rcvMsgA0 = ChatMessage(from: ChatContact(handle: "m1@spokechat.ch"    ) , to: ChatContact(handle: "manuel@spokechat.ch"), body: blitzkriegBob[0], created: Date.today.noon)
            let rcvMsgA1 = ChatMessage(from: ChatContact(handle: "m1@spokechat.ch"    ) , to: ChatContact(handle: "manuel@spokechat.ch"), body: blitzkriegBob[1], created: Date.today.noon.dateByAdding(unit: .second, offset: 3))
            let sndMsgA0 = ChatMessage(from: ChatContact(handle: "manuel@spokechat.ch") , to: ChatContact(handle: "m1@spokechat.ch"    ), body: blitzkriegBob[2], created: Date.today.noon.dateByAdding(unit: .second, offset: 6))
            let sndMsgA1 = ChatMessage(from: ChatContact(handle: "manuel@spokechat.ch") , to: ChatContact(handle: "m1@spokechat.ch"    ), body: blitzkriegBob[3], created: Date.today.noon.dateByAdding(unit: .second, offset: 9))
            let rcvMsgA2 = ChatMessage(from: ChatContact(handle: "m1@spokechat.ch"    ) , to: ChatContact(handle: "manuel@spokechat.ch"), body: blitzkriegBob[4], created: Date.today.noon.dateByAdding(unit: .second, offset: 15))
            let sndMsgA2 = ChatMessage(from: ChatContact(handle: "manuel@spokechat.ch") , to: ChatContact(handle: "m1@spokechat.ch"    ), body: blitzkriegBob[5], created: Date.today.noon.dateByAdding(unit: .second, offset: 16))
            let rcvMsgB0 = ChatMessage(from: ChatContact(handle: "m2@spokechat.ch"    ) , to: ChatContact(handle: "manuel@spokechat.ch"), body: petcementary[0] , created: Date.today.noon)
            let sndMsgB0 = ChatMessage(from: ChatContact(handle: "manuel@spokechat.ch") , to: ChatContact(handle: "m2@spokechat.ch"    ), body: petcementary[1] , created: Date.today.noon.dateByAdding(unit: .second, offset: 9))
            
            beforeEach {
                self.appStateAccessor = AppStateAccessor(appstateStore:AppStateStore())
                let gateways = mockGateways(appStateAccessor: self.appStateAccessor)
                self.messageGatewayMock = gateways.messaging as? MessageSendingGatewayMock
                self.loginGatewayMock = gateways.login as? LoginGatewayMock
                self.addContactGatewayMock = gateways.addContact as? AddContactGatewayMock
                self.app = createAppCore(receivers: [], gateways: gateways, appStateAccessor: self.appStateAccessor, rootHandler: { self.app?($0)})
            }
            
            afterEach {
                self.app = nil
                self.appStateAccessor = nil
                self.messageGatewayMock = nil
                self.loginGatewayMock = nil
            }
            
            describe("Sending") {
                describe("a message") {
                    describe("after successfully logging in") {
                        beforeEach { self.login(successfully: true) }
                        
                        it("will be added to a new chat") {
                            self.app(.chat(.message(.send(sndMsgA0))))
                            
                            expect(self.appstateStore.state.currentChats).toEventually(haveCount(1))
                            expect(self.appstateStore.state.currentChats.last?.messages.last).toEventually(equal(sndMsgA0))
                        }
                    }
                    describe("after failed login") {
                        beforeEach { self.login(successfully: false) }
                        
                        it("will not be added to a new chat"){
                            self.app(.chat(.message(.send(sndMsgA0))))
                            
                            expect(self.appstateStore.state.currentChats).toEventually(haveCount(0))
                        }
                    }
                }
            }
            describe ("Receiving"){
                beforeEach { self.login(successfully: true) }
                
                describe("a message") {
                    it("it will be added to a new chat") {
                        self.app(.chat(.message(.received(rcvMsgA0))))
                        
                        expect(self.appstateStore.state.currentChats).toEventually(haveCount(1))
                        expect(self.appstateStore.state.currentChats.last?.messages.last).toEventually(equal(rcvMsgA0))
                    }
                }
                describe("two messages") {
                    describe("from same sender"){
                        it("will add them to the same chat"){
                            self.app(.chat(.message(.received(rcvMsgA0))))
                            self.app(.chat(.message(.received(rcvMsgA1))))
                            
                            expect(self.appstateStore.state.currentChats).toEventually(haveCount(1))
                            expect(self.appstateStore.state.currentChats.last?.messages).toEventually(equal([rcvMsgA0, rcvMsgA1]))
                        }
                    }
                    describe("from different senders") {
                        it("will add them to different chats") {
                            self.app(.chat(.message(.received(rcvMsgA0))))
                            self.app(.chat(.message(.received(rcvMsgB0))))
                            
                            expect(self.appstateStore.state.currentChats).toEventually(haveCount(2))
                            expect(self.appstateStore.state.currentChats.secondToLast?.messages).toEventually(equal([rcvMsgA0]))
                            expect(self.appstateStore.state.currentChats        .last?.messages).toEventually(equal([rcvMsgB0]))
                        }
                    }
                }
            }
            describe("Exchanging messages") {
                beforeEach { self.login(successfully: true) }
                
                describe("with same receiver"){
                    it("will add messages to the same chats") {
                        self.app(.chat(.message(.received(rcvMsgA0))))
                        self.app(.chat(.message(.send(sndMsgA0))))
                        
                        expect(self.appstateStore.state.currentChats).toEventually(haveCount(1))
                        expect(self.appstateStore.state.currentChats.last?.messages).toEventually(equal([rcvMsgA0, sndMsgA0]))
                    }
                }
                describe("with different receivers") {
                    it("will add messages to different chats"){
                        self.app(.chat(.message(.received(rcvMsgA0))))  // <- Chat A
                        self.app(.chat(.message(.received(rcvMsgA1))))  // <- Chat A
                        self.app(.chat(.message(    .send(sndMsgA0))))  // -> Chat A
                        self.app(.chat(.message(    .send(sndMsgA1))))  // -> Chat A
                        self.app(.chat(.message(.received(rcvMsgB0))))  // <- Chat  B
                        self.app(.chat(.message(.received(rcvMsgA2))))  // <- Chat A
                        self.app(.chat(.message(    .send(sndMsgA2))))  // -> Chat A
                        self.app(.chat(.message(    .send(sndMsgB0))))  // -> Chat  B
                        
                        expect(self.appstateStore.state.currentChats).toEventually(haveCount(2))
                        expect(self.appstateStore.state.currentChats.secondToLast?.messages).toEventually(equal([rcvMsgA0, rcvMsgA1, sndMsgA0, sndMsgA1, rcvMsgA2, sndMsgA2]))
                        expect(self.appstateStore.state.currentChats        .last?.messages).toEventually(equal([rcvMsgB0, sndMsgB0]))
                    }
                }
            }
        }
        context("Contact") {
            beforeEach {
                self.appStateAccessor = AppStateAccessor(appstateStore:AppStateStore())
                let gateways = mockGateways(appStateAccessor: self.appStateAccessor)
                self.messageGatewayMock = gateways.messaging as? MessageSendingGatewayMock
                self.loginGatewayMock = gateways.login as? LoginGatewayMock
                
                self.app = createAppCore(receivers: [], gateways: gateways, appStateAccessor: self.appStateAccessor, rootHandler: { self.app?($0)})
            }
            afterEach {
                self.app = nil
                self.appStateAccessor = nil
                self.messageGatewayMock = nil
                self.loginGatewayMock = nil
            }
            
            describe("Adding") {
                beforeEach { self.login(successfully: true) }
                
                describe("a contact") {
                    describe("successfully "){
                        it("saves it to the state") {
                            self.app(.contact(.request(.addContact(ChatContact(handle: "abc@spokechat.ch")))))
                            
                            expect(self.appstateStore.state.chatContacts).toEventually(haveCount(1))
                            expect(self.appstateStore.state.chatContacts.last?.handle).toEventually(equal("abc@spokechat.ch"))
                        }
                    }
                }
            }
            
            describe("Receiving") {
                beforeEach { self.login(successfully: true) }
                
                describe("a contact request") {
                    it("saves it to the state") {
                        self.app(.contact(.response(.addContactRequestReceived(ChatContact(handle: "abc@spokechat.ch")))))
                        
                        expect(self.appstateStore.state.unapproved).toEventually(haveCount(1))
                        expect(self.appstateStore.state.unapproved.last?.handle).toEventually(equal("abc@spokechat.ch"))
                    }
                }
                
                describe("an updated contact") {
                    let old = ChatContact(handle: "abc@spokechat.ch").alter(.nickname("Billy"))
                    let new = old.alter(.nickname("Billy Jean"))

                    it("will be saved to state") {
                        self.app(.contact(.updated(new)))
                        expect(self.appstateStore.state.chatContacts).toEventually(haveCount(1))
                        expect(self.appstateStore.state.chatContacts.last?.handle).toEventually(equal("abc@spokechat.ch"))
                        expect(self.appstateStore.state.chatContacts.last?.nickname).toEventually(equal("Billy Jean"))
                    }
                }
            }
            
            describe("Changing") {
                let old = ChatContact(handle: "abc@spokechat.ch").alter(.nickname("Billy"))
                let new = old.alter(.nickname("Billy Jean"))
                
                beforeEach { self.login(successfully: true) }

                describe("nickname") {
                    
                    beforeEach {
                        self.app(.contact(.request(.addContact(old))))
                    }
                    it("by replacing old contact with new"){
                        self.app(.contact(.request(.replace((contact: old, with: new)))))
                        
                        expect(self.appstateStore.state.chatContacts).toEventually(haveCount(1))
                        expect(self.appstateStore.state.chatContacts.last?.handle).toEventually(equal("abc@spokechat.ch"))
                        expect(self.appstateStore.state.chatContacts.last?.nickname).toEventually(equal("Billy Jean"))
                    }
                }
            }
        }
    }
    
    private func login(successfully succees: Bool) {
        self.loginGatewayMock.shallFail = !succees
        self.app(.chat(.request(.login(ChatCredentials(username: "manuel@spokechat.ch", password: "forgot!")))))
    }
}


//MARK: - Tools

extension ChatMessage: CustomStringConvertible {
    public var description: String { self.body }
}

extension Array {
    func last(offset n: Array.Index) -> Array.Element? {
        n < count
            ? self[count - 1 - n]
            : nil
    }
    
    var secondToLast: Array.Element? { last(offset: 1) }
}

func dateComps(second: Int) -> DateComponents {
    var dc = DateComponents()
    dc.second = second
    return dc
}

let cal = Calendar.current

extension Date {
    static var     today: Date { Date().midnight      }
    static var yesterday: Date { Date.today.dayBefore }
    static var  tomorrow: Date { Date.today.dayAfter  }
    
    var dayBefore: Date { cal.date(byAdding: .day, value: -1, to: self)! }
    var  dayAfter: Date { cal.date(byAdding: .day, value:  1, to: self)! }
    var  midnight: Date { cal.date(bySettingHour:  0, minute: 0, second: 0, of: self)!}
    var      noon: Date { cal.date(bySettingHour: 12, minute: 0, second: 0, of: self)!}
    
    func dateByAdding(unit: Calendar.Component, offset: Int) -> Date {
        cal.date(byAdding: unit, value:  offset, to: self)!
    }
    
    private var cal: Calendar { Calendar.current }
}
